package ru.andrey.dev;

import java.util.Scanner;

/**
 * Класс, который определяет, хватит ли денег на покупку джинс со скидкой.
 *
 * @author Рыжкин Андрей.
 * @version 1.3
 * @since 21.09.2016
 */
public class Jeans {
    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);

        int money;
        int price;
        int discount;

        System.out.print("Количество денег: ");
        money = reader.nextInt();
        while (money <= 0) {
            System.out.println("Неверное значение!");
            System.out.print("Количество денег: ");
            money = reader.nextInt();
        }

        System.out.print("Цена джинс: ");
        price = reader.nextInt();

        while (price < 0) {
            System.out.println("Неверное значение!");
            System.out.print("Цена джинс: ");
            price = reader.nextInt();
        }

        System.out.print("Скидка: ");
        discount = reader.nextInt();

        while (discount > 100 || discount < 0) {
            System.out.println("Неверное значение");
            System.out.print("Скидка: ");
            discount = reader.nextInt();
        }

        if (discountPrice(price, discount) <= money) {
            System.out.println("Хватит денег!");
        } else {
            System.out.println("НЕ хватит денег!");
        }
    }

    /**
     * Метод нахождения цены джинс со скидкой.
     *
     * @param price    цена джинс.
     * @param discount скидка.
     * @return возращает цену со скидкой.
     */
    public static double discountPrice(int price, int discount) {
        if (discount == 0) {
            return price;
        } else {
            return price - (discount / 100.0 * price);
        }
    }
}